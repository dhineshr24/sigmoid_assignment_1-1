package assignment_1;

import org.apache.commons.math3.util.Pair;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import util.AssignmentUtil;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class HDFSUtil {

    public static void makeDirHDFS(Configuration conf, String dirName) throws IOException {
        FileSystem fileSystem = FileSystem.get(conf);
        String fullDir = "/" + dirName;
        Path path = new Path(fullDir);
        if (fileSystem.exists(path)) {
            System.out.println("LOG_MKDIR: This Directory already exists\n");
            return;
        }
        fileSystem.mkdirs(path);
        System.out.println("LOG_INFO: Directory created successfully!\n");
        fileSystem.close();
    }

    private static List<Pair<byte[], Integer>> readSingleSourceFile(String source) throws IOException {
        InputStream in = new BufferedInputStream(new FileInputStream(source));
        byte[] b = new byte[1024];
        List<Pair<byte[], Integer>> sourceContents = new ArrayList<>();
        int numBytes;
        while ((numBytes = in.read(b)) > 0) {
            Pair<byte[], Integer> pair = new Pair<>(b, numBytes);
            sourceContents.add(pair);
        }
        in.close();
        return sourceContents;
    }


    private static boolean writeSingleSourceFileTOHDFS(List<Pair<byte[], Integer>> sourceFile, String destination, FileSystem fileSystem) throws IOException {
        Path path = new Path(destination);
        if(fileSystem.exists(path)) {
            System.out.println("LOG_COPY: " + "File " + destination + " already exists!");
            return false;
        }

        FSDataOutputStream out = fileSystem.create(path);
        for (Pair<byte[], Integer> record : sourceFile) {
            out.write(record.getFirst(), 0, record.getSecond());
        }
        out.close();
        return true;
    }


    public static void copyAllFilesFromLocalToHDFS(Configuration conf, String baseSource, String baseDestination) throws IOException {
        FileSystem fileSystem = FileSystem.get(conf);
        int noOfFiles = AssignmentUtil.noOfFilesInDirectory(baseSource);
        int currentFileNumber = 0;

        while (currentFileNumber < noOfFiles) {
            currentFileNumber++;
            String source = baseSource + "/Employee_details_" + currentFileNumber + ".csv";
            List<Pair<byte[], Integer>> sourceFileContent = readSingleSourceFile(source);
            String fileName = source.substring(source.lastIndexOf('/') + 1);

            if (baseDestination.charAt(baseDestination.length() - 1) != '/') {
                baseDestination = baseDestination + "/" + fileName;
            } else {
                baseDestination = baseDestination + fileName;
            }

            boolean copySuccessFull = writeSingleSourceFileTOHDFS(sourceFileContent, baseDestination, fileSystem);
            if (copySuccessFull) {
                System.out.println("LOG_COPY: " + "Copied file " + fileName + " successfully!");
            }

        }
        System.out.println("LOG_INFO: All files copied successfully!\n");
        fileSystem.close();

    }
}
